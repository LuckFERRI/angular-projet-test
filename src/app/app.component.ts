import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Animals';

  selectionAnimal = null;

  select(animal){
    this.selectionAnimal = animal
  }
 animals=[
{
  name : "Beluga",
  description : "Le béluga ou bélouga (Delphinapterus leucas), appelé également baleine blanche, dauphin blanc et marsouin blanc, est une espèce de cétacés de la famille des Monodontidae vivant dans l'océan Arctique. Il dispose d'un des sonars les plus sophistiqués de tous les cétacés. Ce sonar lui est indispensable pour s'orienter et se repérer dans les canaux de glace immergés, qui forment un véritable labyrinthe.",
  img : "/assets/img/beluga.jpg",
},
{
  name : "Gypaèthe Barbu",
  img : '/assets/img/gypaethe-barbu.jpg',
  description : "Le Gypaète barbu (Gypaetus barbatus) est la seule espèce du genre Gypaetus. Cette espèce est présente en Asie centrale, en Afrique, au Moyen-Orient et en Europe, dont elle est l'une des quatre espèces de vautours — elle s'y cantonne principalement aux Pyrénées, aux Alpes, au massif Corse et à la Crète. Il appartient à l'ordre des Accipitriformes et à la famille des Accipitridés."
},
{
  name : "Mamba",
  img : '/assets/img/mamba.jpg',
  description : "Dendroaspis est un genre de serpents de la famille des Elapidae1. En français, les espèces de ce genre sont appelées Mamba."
},

{
  name : "Koala",
  img : "/assets/img/koala.jpg",
  description : "Le koala (Phascolarctos cinereus), appelé aussi Paresseux australien, est une espèce de marsupial arboricole herbivore endémique d'Australie et le seul représentant encore vivant de la famille des Phascolarctidés. On le trouve dans les régions côtières de l'Australie méridionale et orientale, d'Adélaïde à la partie sud de la péninsule du cap York. Les populations s'étendent aussi sur des distances considérables dans l'arrière-pays australien (outback), là où l'humidité est suffisante pour le maintien de forêts. Les koalas d'Australie-Méridionale furent exterminés au début du xxe siècle, mais cet État fédéré a depuis été repeuplé grâce à des transferts du Victoria. Cet animal n'est présent ni en Tasmanie ni en Australie-Occidentale."        
}
]

}