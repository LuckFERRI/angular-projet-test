import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TD1Component } from './td1.component';

describe('TD1Component', () => {
  let component: TD1Component;
  let fixture: ComponentFixture<TD1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TD1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TD1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
